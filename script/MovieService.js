(function() {

    var MovieService = function($http) {

        var getMovieInfo = function(title) {
            return $http.get("http://www.omdbapi.com/?t=" +title+
            "&y=&plot=short&r=json").then(function(response) {
                return response.data;
            });
        };

        return {
            getMovieInfo: getMovieInfo
        };
    };

    var module = angular.module("minApp");
    module.factory("MovieService", MovieService);

}());
