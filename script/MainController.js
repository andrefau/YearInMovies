(function() {
    var app = angular.module("minApp");

    var MainController = function($scope, MovieService) {
        $scope.message = "YEAR IN MOVIES";

        var onMovieComplete = function(data) {
            $scope.resultat = data;
        };

        var onError = function(reason) {
            $scope.error = "Kunne ikke finne filmen";
        };

        $scope.search = function(moviename) {
            MovieService.getMovieInfo(moviename).then(onMovieComplete, onError);
        };
    };

    app.controller("MainController", MainController);
}());
